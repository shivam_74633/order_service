'use strict';

const ApiError = require('../ApiError');
const _ = require('lodash');
/**
 * Express error handler middleware.
 * Should be the last middleware used.
 * 
 * @param {Error|*} err Error value 
 * @param {Request} req Express request
 * @param {Response} res Express response
 * @param {Function} next Next function
 */

const errorHandler = async (err, req, res ,next) => {
    console.log('@@@@',err)
    // End response if headers are already sent
    if(res.headersSent) {
        res.end();
    }
    else if (err instanceof ApiError) {
        // Sending Api error response
        err.send(res);
    }

    else if (err instanceof Error){
        new ApiError({
            status: err.status,
            message: err.status ==400 || 401 ?'Validation Error': 'Internal Server Error.',
            code: err.code ||inspectDetail(err)
        }).send(res);
    }

    else{
        new ApiError({
            status: err.status,
            message: err.status ==400 || 401 ?'Validation Error': 'Internal Server Error.',
            code: err.code ||inspectDetail(err)
        }).send(res);
    }

    next();
}


/**
 * 
 * @param {*} err 
 */
 const inspectDetail = (err) => {

    if (err instanceof Error && _.has(err, 'message')) {
        return err.message;
    }

    return 'Unknown Error';
}


module.exports = errorHandler;