module.exports = {
    authHandler: require('./authHandler'),
    errorHandler: require('./errorHandler'),
    imageUpload: require('./imageUpload')
}