const {errors} = require('../../constants');
const ApiError = require('../ApiError');
const {jwt} = require('../utils/index');
const {redis} = require('../services');



module.exports = async (req) => {
    try{
        const token = req.headers['authorization'].split(' ')[1];
        const tokenPayload = await jwt.verifyAuthToken(token,process.env.SECRET_KEY_ACESS);
        if(!tokenPayload) return false
        if(await redis.checkRefreshTokenPresent(tokenPayload.userId,tokenPayload.tokenId)) {
            req.body.tokenId = tokenPayload.tokenId;
            req.body.userId = tokenPayload.userId;
            return true
        }
        return false
    }
    catch (err) {
        throw new ApiError(errors.SOMETHING_WENT_WRONG);
    }
}