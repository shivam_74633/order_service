'use strict';

const AWS = require('aws-sdk');


const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET
});


const FILE_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpeg',
    'image/jpg': 'jpg',
}


const isValidImage = (mimeType) => {
    if(FILE_TYPE_MAP[mimeType]) {
        return FILE_TYPE_MAP[mimeType]
    }
    return null
}


const uploadtoAws = async (file,name) => {
    let params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `images/${name}-${Date.now().toString()}`,
        Body: Buffer.from(file.buffer,file.encoding),
        ContentType: file.mimetype
    }
    const res = await s3.upload(params).promise();
    return res.Location
}


const uploadSingleImage = async (req,res) => {
    const file = req.files[0];
    const extension = isValidImage(file.mimetype);
    if(file.fieldname == 'image' && extension) {
        const uploadedImage = await uploadtoAws(file,req.body.name);
        return uploadedImage
    } else {
        res.send('No Image found or unsupported fromat');
    }
}



module.exports = {uploadSingleImage}