'use strict';

const jwt = require('jsonwebtoken');

const verifyAuthToken = (token,secretKey) => {
    return new Promise((resolve,reject) => {
        jwt.verify(token,secretKey, (err,decoded) => {
            if(err) return resolve(null);
            return resolve(decoded);
        })
    });
}



module.exports = {
    verifyAuthToken
}