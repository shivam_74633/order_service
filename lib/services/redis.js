'use strict';

const Redis = require('redis');

const { promisify } = require('util');

const client = Redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
});

//For Adding and getting Data
const GET_TOKEN = promisify(client.lrange).bind(client);

const checkRefreshTokenPresent = async (key,tokenID) => {
    const tokensFromRedis = await GET_TOKEN(key,0,-1);
    if(tokensFromRedis.includes(tokenID)) return true
    else return false;
}


module.exports = {  
    checkRefreshTokenPresent,
}