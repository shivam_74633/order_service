/**
 * @class
 * API Error class.
 */
 module.exports = class ApiError extends Error {

    /**
     * Constructor
     * @param {string} message error message
     */
    constructor(err) {
        super(err.detail || err.message);
        this._status = err.status;
        this._code = err.code;
    }

    /**
     * Sends error JSON to response stream.
     * @param {Response} res Server response.
     */
    send(res) {
        // set status
        res.status(this._status || 500);

        // send JSON
        res.json({
            isError: true,
            code: this._code || 'INTERNAL_ERROR',
            message: this.message
        });
    }
}