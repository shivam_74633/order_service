'use strict';

const express = require('express');
const app = express();
require('dotenv').config({path: `./env/.env.${app.get('env')}`});

app.set('host', process.env.SERVER_HOST);
app.set('port', process.env.SERVER_PORT);

require('./src/boot')(__dirname, app)
    .then(() => {
        app.listen(app.get('port'), () => {
            console.log(`App(🌎) is running at http://${app.get('host')}:${app.get('port')} in ${app.get('env')} mode ✓.`);
        })
    })
    .catch((err) => {
        console.log(err.message);   
    })