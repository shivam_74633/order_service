'use strict';

const cors = require('cors');
const express = require('express');
const {errorHandler,authHandler} = require('../../lib').middlewares;
const path = require('path');
const OpenApiValidator = require('express-openapi-validator');
const swaggerUI = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerJSDocs = YAML.load('./apiDocs/api.yaml');
const apiSpec = './apiDocs/api.yaml';



const configureAPI = async (app,rootDirectory) => {
    app.use((req, res, next) => {
		console.log(new Date().toISOString(), '=', req.method, '=', req.originalUrl);

		return next();
	});

    app.use(
		cors(['*'])
	);


    app.use('/api', express.json());

	app.use('/api/docs',swaggerUI.serve,swaggerUI.setup(swaggerJSDocs));


	app.use(
		OpenApiValidator.middleware({
			apiSpec,
			validateResponses: true, 
			operationHandlers: path.join(rootDirectory,process.env.ROUTE_PATH), 
			validateSecurity: {
			  handlers: {
				async bearerAuth(req, scopes) {
				  return await authHandler(req)
				}
			  }
			},
		  }),
	)

	app.use(errorHandler);

}

module.exports = configureAPI;