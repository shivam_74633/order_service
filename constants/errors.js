module.exports = {
    //4xx Client Error

    //400 BadRequest
    //Invalid_Request
    //404 Not Found
    INVALID_INPUT: {
        status: 400,
        code: 'INVALID_INPUT',
        detail: 'The request input is not as expected.',
        desc: 'Bad Request'
    },
    MAXIMUM_DEVICE_LIMIT: {
        status: 400,
        code: 'MAXIMUM_DEVICE_LIMIT',
        detail: 'The maximum device login limit is exceeded.',
        desc: 'Maximum Device Limit'
    },
    //AccessDenied 403 Forbidden
    //AlreadyExists
    //409 Conflict
    //Internal Server Error
    //UnauthorisedAccess
    UNAUTHORIZED: {
        status: 401,
        code: 'EXPIRED_TOKEN',
        detail: 'UNAUTHORIZED USER',
        desc: 'Auth Token Expired.'
    },
    
    INVALID_TOKEN: {
        status: 401,
        code: 'INVALID_OR_EXPIRED_TOKEN',
        detail: 'Access denied. Current user does not has access for this resource.',
        desc: 'Auth Token Invalid or Expired'
    },
    MISSING_TOKEN: {
        status: 401,
        code: 'MISSING_TOKEN',
        detail: 'Auth Token Missing in request.',
        desc: 'Auth Token Missing'
    },
    INVALID_KEY: {
        status: 403,
        code: 'INVALID_KEY',
        detail: 'Your request did not include an API key.',
        desc: 'API Key Invalid'
    },
    MISSING_KEY: {
        status: 403,
        code: 'MISSING_KEY',
        detail: 'Your request did not include an API key.',
        desc: 'API Key Missing'
    },
    EMAIL_NOT_REGISTERED: {
        status: 404,
        code: 'EMAIL_NOT_REGISTERED',
        detail: 'The email address you entered isn"t connected to an account.',
        desc: 'email not registered'
    },

    //5xx Server Error
    INTERNAL_ERROR: {
        status: 500,
        code: 'INTERNAL_ERROR',
        detail: 'An unexpected internal error has occurred. Please contact Support for more information.',
        desc: 'Internal Server Error'
    },


    SOMETHING_WENT_WRONG: {
        status: 500,
        code: 'SOMETHING_WENT_WRONG',
        detail: 'Something Went Wrong.',
        desc: 'Internal Server Error'
    },
    IMAGE_UPLOAD_ERROR: {
        status: 400,
        code: 'SOMETHING_WENT_WRONG',
        detail: 'Failed to upload image.',
        desc: 'IMAGE_UPLOAD_ERROR:'
    },
    UNABLE_TO_SAVE_PRODUCT: {
        status: 500,
        code: 'SOMETHING_WENT_WRONG',
        detail: 'Unable to save product try again after some time.',
        desc: 'UNABLE_TO_SAVE_PRODUCT'
    },

    
    ID_NOT_FOUND: {
        status: 404,
        code: 'ID_NOT_FOUND',
        detail: 'ID is not registered.',
        desc: 'Resource Not Found'
    },
    PASSWORD_EMAIL_MISMATCH: {
        status: 401,
        code: 'PASSWORD_EMAIL_MISMATCH',
        detail: 'Email id or password is incorrect.',
        desc: 'Unauthorized'
    },
    WRONG_PASSWORD: {
        status: 400,
        code: 'PASSWORD_MISMATCH',
        detail: 'Password is incorrect.',
        desc: 'Incorrect Password'
    },
    SAME_AS_OLD_PASSWORD: {
        status: 400,
        code: 'SAME_PASSWORD',
        detail: 'New password is same as old password',
        desc: 'Please enter a new password.'
    },
    


    OTP_MISMATCH: {
        status: 401,
        code: 'OTP_MISMATCH',
        detail: 'Your otp did not match.',
        desc: 'Unauthorized'
    },
    OTP_ALREADY_SENT: {
        status: 400,
        code: 'OTP_ALREADY_SENT',
        detail: 'Otp has alredy sent to your mail id please wait for five minutes to resend otp',
        desc: 'Unauthorized'
    },


    ID_EXIST: {
        status: 409,
        code: 'ID_EXIST',
        detail: 'ID already exists. Try another!',
        desc: 'Conflict'
    },

    MOBILE_EXIST: {
        status: 409,
        code: 'MOBILE_EXIST',
        detail: 'Mobile Number already exists. Try another!',
        desc: 'Conflict'
    },

    ACCOUNT_INACTIVE: {
        status: 403,
        code: 'ACCOUNT_INACTIVE',
        detail: 'Your account is inactive. Please contact admin.',
        desc: 'Forbidden'
    },

    INVALID_DATE: {
        status: 403,
        code: 'INVALID_DATE',
        detail: 'Date is lesser than today is not allowed.',
        desc: 'Forbidden'
    },

}